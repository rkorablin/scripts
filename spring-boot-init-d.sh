#!/usr/bin/env bash

project=$1

echo "$project"

rm -rfv output/"$project"
mkdir -p output/"$project"

mkdir -p output/"$project"/logs
mkdir -p output/"$project"/pid
mkdir -p output/"$project"/for-deploy

touch output/"$project"/"$project".conf
echo 'JAVA_OPTS="-Xms64m -Xmx64m -Dspring.profiles.active=prod"' >> output/"$project"/"$project".conf
echo 'PID_FOLDER=/home/'"$project"'/pid'  >> output/"$project"/"$project".conf
echo 'LOG_FOLDER=/home/'"$project"'/logs' >> output/"$project"/"$project".conf

touch output/"$project"/deploy.sh
echo '#!/usr/bin/env bash' >> output/"$project"/deploy.sh
echo 'cd' >> output/"$project"/deploy.sh
echo '/etc/init.d/'"$project"' stop' >> output/"$project"/deploy.sh
echo 'rm -fv '"$project"'.jar' >> output/"$project"/deploy.sh
echo 'cp -v for-deploy/'"$project"'.jar .' >> output/"$project"/deploy.sh
echo 'chmod +x '"$project"'.jar' >> output/"$project"/deploy.sh
echo '/etc/init.d/'"$project"' start' >> output/"$project"/deploy.sh >> output/"$project"/deploy.sh
chmod +x output/"$project"/deploy.sh